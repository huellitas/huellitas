import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/Primero';
import LinksScreen from '../screens/Segundo';
import CodigoQRScreen from '../screens/scannerQR';
//import SettingsScreen from '../screens/SettingsScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Inicio de sesión',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={
        Platform.OS === 'ios'
          ? `ios-information-circle${focused ? '' : '-outline'}`
          : 'md-information-circle'
      }
    />
  ),
};

const LinksStack = createStackNavigator({
  Links: LinksScreen,
});

LinksStack.navigationOptions = {
  tabBarLabel: 'Registro',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-log-in' : 'md-log-in'}
    />
  ),
};

/*
const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-options' : 'md-options'}
    />
  ),
};
*/

const CodigoQRStack = createStackNavigator({
  CodigoQR: CodigoQRScreen,
});

CodigoQRStack.navigationOptions = {
  tabBarLabel: 'CodigoQR',
  tabBarIcon: ({ focused }) => (
    <TabBarIcon
      focused={focused}
      name={Platform.OS === 'ios' ? 'ios-qr-scanner' : 'md-qr-scanner'}
    />
  ),
};

const primeroHome = createBottomTabNavigator({
  HomeStack,
  LinksStack,
})

const despues = createBottomTabNavigator({
  CodigoQRStack,
},
{
  headerMode: 'none',
  navigationOptions: {headerVisible: false,}
})

export default createStackNavigator({
  primeroHome,
  despues,
  //SettingsStack,
},
{
  headerMode: 'none',
  navigationOptions: {headerVisible: false,}
});
